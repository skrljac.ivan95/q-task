=== Q task theme ===

Made on empty theme by UNDERSCORES (https://underscores.me/);

Files worked on:

----------------------
JS:

js/auth.js
js/quoteCustomBlock

----------------------
PHP:

plugins/movies/movies.php

tpl-home.php
single-movie.php
functions.php

----------------------


INSTALATION

1. Download and place theme inside your wordpress theme folder or copy only the files listed above under "Files worked on" into your WordPress theme folder.
2. From the theme directory, copy the "plugins" folder and place it outside of the theme folder. Alternatively, you can copy just the "movies" folder and add it to your existing plugins folder in your WP installation.
3. Log in to your WordPress admin panel. Navigate to the "Appearance" section and activate the downloaded theme.
4. Go to "Pages" and select the page you want to set as your homepage, or create a new one. On the right side, choose the "Home" template and update the page.
5. In the WordPress admin panel, navigate to "Settings" and click on "Reading." Under "Your homepage displays," select "A static page." Choose the page you configured as your homepage in step 4 from the "Homepage" dropdown menu.
6. Go to "Settings" and click on "Permalinks." Under "Permalink structure," select "Post name," and save your changes.
7. In the "Plugins" section of your WordPress admin panel, activate the "Movies" plugin.
8. Verify the Custom Post Type: After activating the "Movies" plugin, you should see a custom post type named "Movies" in your WordPress admin panel.
9. Inside Custom Post Type click "add movie" and fill content. Plugin makes custom field for movie title at bottom and you can add custom block in Gutenberg editor.

--------------------------------------------------


Conclusion:

Both tasks have been completed, including the third "advanced task." However, the "advanced task" was confusing because I didn't understand what the desired outcome of that task was.