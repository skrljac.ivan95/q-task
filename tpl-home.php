<?php
//Template name: Home
get_header(); ?>
 <h1>Login to save token in Cookies</h1>
    <form id="loginForm">
        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required><br><br>

        <label for="password">Password:</label>
        <input type="password" id="password" name="password" required><br><br>

        <button type="submit">Login</button>
    </form>
<?php get_footer(); ?>