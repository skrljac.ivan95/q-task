function handleLoginFormSubmit(){

    //check if form exist
    const form = document.querySelectorAll("#loginForm");

    if(!form){
        return;
    }

    //forEach form element
    form.forEach((element)=> {
        element.addEventListener("submit", function (e) {
            e.preventDefault();

            //get form values
            const email = document.getElementById("email").value;
            const password = document.getElementById("password").value;

            if(!email || !password) {
                alert("Email or password missing");
                return;
            }

            //if there is token run function to save it
            getToken(email,password).then(response => response.json()).then(data=>{
                if (!data.token_key) {
                    alert("Token missing");
                    return;
                }
                saveToken(data);
            })
        });
    });
}

//save Token in Cookies function
function saveToken(data){
    const expirationTime = new Date();
    expirationTime.setSeconds(expirationTime.getSeconds() + 120);
    document.cookie = `Q-token=${data.token_key}; expires=${expirationTime.toUTCString()}; path=/`;
}

// Fetch api token results
function getToken(email,password){
    const apiUrl = 'https://symfony-skeleton.q-tests.com/api/v2/token';
    //Fetch API
    return fetch(apiUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
    })
}

handleLoginFormSubmit();