const { registerBlockType } = wp.blocks;
const { TextControl } = wp.components;
const { createElement } = wp.element;

registerBlockType('q-task/favorite-movie-quote', {
    title: 'Favorite Movie Quote',
    icon: 'format-quote',
    category: 'common',
    attributes: {
        quote: {
            type: 'string',
            default: '',
        },
    },
    edit: function(props) {
        const { attributes, setAttributes } = props;
        console.log('Attributes:', attributes);
        return createElement(
            'div',
            null,
            createElement(
                TextControl,
                {
                    label: 'Enter your favorite movie quote:',
                    value: attributes.quote,
                    onChange: (newQuote) => setAttributes({ quote: newQuote }),
                }
            )
        );
    },
    save: function() {
        return null;
    },
});