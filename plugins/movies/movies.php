<?php
/*
Plugin Name: Movies
Description: A WordPress plugin build for Q agency task.
Version: 1.1
*/

function custom_post_type_register() {
    $labels = array(
        'name' => 'Movies',
        'singular_name' => 'Movie',
        'add_new' => 'Add New Movie',
        'add_new_item' => 'Add New Movie',
        'edit_item' => 'Edit Movie',
        'new_item' => 'New Movie',
        'view_item' => 'View Movie',
        'search_items' => 'Search Movies',
        'not_found' => 'No movies found',
        'not_found_in_trash' => 'No movies found in Trash',
        'parent_item_colon' => 'Parent Movie:',
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'publicly_queryable' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'movies'),
        'capability_type' => 'post',
        'hierarchical' => false,
        'supports' => array(
            'title',
            'editor',
            'thumbnail',
            'excerpt',
        ),
        'show_in_rest' => true,
        'rest_base' => 'movies',
    );

    register_post_type('movie', $args);
}
add_action('init', 'custom_post_type_register');

function add_movie_title_meta_box() {
    add_meta_box(
        'movie_title',
        'Movie Title',
        'movie_title_meta_box_callback',
        'movie',
        'normal',
        'default'
    );
}
add_action('add_meta_boxes', 'add_movie_title_meta_box');

function movie_title_meta_box_callback($post) {
    $movie_title = get_post_meta($post->ID, 'movie_title', true);
    ?>
    <input type="text" name="movie_title" id="movie_title" value="<?php echo esc_attr($movie_title); ?>" style="width: 100%;" />
    <?php
}

function save_movie_title_meta($post_id) {
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
    if (!current_user_can('edit_post', $post_id)) return;

    if (isset($_POST['movie_title'])) {
        update_post_meta($post_id, 'movie_title', sanitize_text_field($_POST['movie_title']));
    }
}
add_action('save_post', 'save_movie_title_meta');
?>