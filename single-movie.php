<!-- Template for movie post from Q task -->
<?php
get_header();
$movie_title = get_post_meta(get_the_ID(), 'movie_title', true);
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <?php while (have_posts()) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <header class="entry-header">
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </header>
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>
            </article>
            <?php if (!empty($movie_title)) : ?>
                <div class="movie-title">
                    <strong>Movie Title:</strong> <?php echo esc_html($movie_title); ?>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    </main>
</div>

<?php
get_footer();
?>